variable "VM_NAME" {
  default = "IaC"
}

variable "VM_ADMIN" {
  default = "admin-iac"
}

variable "LOCATION" {
  default = "West Europe"
}

variable "resource_group_name" {
  default = "tfstate"
}

variable "storage_account_name" {
  default = "tfstatejgp67"
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

# variable "tenant_id" {
# }

# variable "client_id" {
# }

# variable "client_secret" {
# }

# variable "subscription_id" {
# }
