terraform plan -out out.plan

terraform apply out.plan

az account set --subscription 1bb659ca-bfc3-49df-96bb-12db71c5fe9f
az aks get-credentials --resource-group azure-k8stest --name k8stest

az aks show --resource-group azure-k8stest --name k8stest --query nodeResourceGroup -o tsv

az disk create \
 --resource-group MC_azure-k8stest_k8stest_westus \
 --name myAKSDisk \
 --size-gb 4 \
 --sku Standard_LRS \
--query id --output tsv

/subscriptions/1bb659ca-bfc3-49df-96bb-12db71c5fe9f/resourceGroups/MC_azure-k8stest_k8stest_westus/providers/Microsoft.Compute/disks/myAKSDisk
