terraform {
  required_version = ">= 1.0.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }

  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstatejgp67"
    container_name       = "tfstate-aks"
    key                  = "codelab.microsoft.tfstate"
  }
}

provider "azurerm" {
  features {}
}

provider "gitlab" {
  base_url = "https://gitlab.com/api/v4/"
  token    = var.epam_token
}
