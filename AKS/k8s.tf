resource "azurerm_resource_group" "k8s" {
  name     = var.resource_group_name
  location = var.location
}

resource "random_id" "log_analytics_workspace_name_suffix" {
  byte_length = 8
}

resource "azurerm_log_analytics_workspace" "test" {
  # The WorkSpace name has to be unique across the whole of azure, not just the current subscription/tenant.
  name                = "${var.log_analytics_workspace_name}-${random_id.log_analytics_workspace_name_suffix.dec}"
  location            = var.log_analytics_workspace_location
  resource_group_name = azurerm_resource_group.k8s.name
  sku                 = var.log_analytics_workspace_sku
}

resource "azurerm_log_analytics_solution" "test" {
  solution_name         = "ContainerInsights"
  location              = azurerm_log_analytics_workspace.test.location
  resource_group_name   = azurerm_resource_group.k8s.name
  workspace_resource_id = azurerm_log_analytics_workspace.test.id
  workspace_name        = azurerm_log_analytics_workspace.test.name

  plan {
    publisher = "Microsoft"
    product   = "OMSGallery/ContainerInsights"
  }
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = azurerm_resource_group.k8s.name
  dns_prefix          = var.dns_prefix

  linux_profile {
    admin_username = "ubuntu"

    ssh_key {
      key_data = var.ssh_public_key
    }
  }
  default_node_pool {
    name       = "agentpool"
    node_count = var.agent_count
    vm_size    = "Standard_D2_v2"
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  addon_profile {
    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = azurerm_log_analytics_workspace.test.id
    }
  }
  network_profile {
    load_balancer_sku = "Standard"
    network_plugin    = "kubenet"
  }

  tags = {
    Environment = "Development"
  }
}

resource "gitlab_project_cluster" "iac" {
  project = var.gitlab_project_iac_id
  name    = "epam-cluster"
  #   domain                        = "example.com"
  enabled                       = true
  kubernetes_api_url            = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  kubernetes_token              = azurerm_kubernetes_cluster.k8s.kube_config.0.password
  kubernetes_ca_cert            = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
  kubernetes_authorization_type = "abac"
  environment_scope             = "*"
  management_project_id         = var.gitlab_project_iac_id
}

resource "gitlab_project_cluster" "diplom" {
  project = var.gitlab_project_diplom_id
  name    = "epam-cluster"
  #   domain                        = "example.com"
  enabled                       = true
  kubernetes_api_url            = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  kubernetes_token              = azurerm_kubernetes_cluster.k8s.kube_config.0.password
  kubernetes_ca_cert            = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
  kubernetes_authorization_type = "abac"
  environment_scope             = "*"
  management_project_id         = var.gitlab_project_diplom_id
}

