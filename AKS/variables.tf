variable "agent_count" {
  default = 3
}

variable "client_id" {
}

variable "client_secret" {
}

variable "ssh_public_key" {
}

variable "dns_prefix" {
  default = "k8stest"
}

variable "cluster_name" {
  default = "k8stest"
}

variable "resource_group_name" {
  default = "azure-k8stest"
}

variable "location" {
  default = "West Europe"
  # default = "West US"
}

variable "log_analytics_workspace_name" {
  default = "testLogAnalyticsWorkspaceName"
}

# refer https://azure.microsoft.com/global-infrastructure/services/?products=monitor for log analytics available regions
variable "log_analytics_workspace_location" {
  default = "eastus"
}

# refer https://azure.microsoft.com/pricing/details/monitor/ for log analytics pricing 
variable "log_analytics_workspace_sku" {
  default = "PerGB2018"
}

variable "epam_token" {
  type = string
}

variable "gitlab_project_iac_id" {
  default = "28745975"
}

variable "gitlab_project_diplom_id" {
  default = "27686930"
}
