resource "azurerm_managed_disk" "disk_dev_psql" {
  name                 = "devAKSDisk"
  location             = var.location
  resource_group_name  = "tfstate"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "4"

  tags = {
    environment = "development"
  }
}

resource "azurerm_managed_disk" "disk_prod_psql" {
  name                 = "prodAKSDisk"
  location             = var.location
  resource_group_name  = "tfstate"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "4"

  tags = {
    environment = "production"
  }
}
