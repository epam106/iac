# Link presentation

https://disk.yandex.ru/i/r0C73j2N5LbBrA


# Create storage account

```
cd ./storageAccount

terraform init

terraform plan -out terraform_azure.tfplan

terraform apply terraform_azure.tfplan

```

# Create VM for IaC

```

cd ./vm-for-runner

terraform init

terraform plan -out terraform_azure.tfplan

terraform apply terraform_azure.tfplan

export ANSIBLE_CONFIG="./ansible-gitlab-runner/ansible.cfg"

ansible-playbook ./ansible-gitlab-runner/docker-iac.yml
ansible-playbook ./ansible-gitlab-runner/kube-iac.yml
```

# Create gitlabUser in Azure

```
# Create a service principal
AKS_SP_APP_ID=$(az ad sp create-for-rbac --name gitlabUser --query appId)

# Retrieve Service principal APPID and Client Secret
AKS_SP_SECRET=$(az ad sp credential reset --name gitlabUser --query "password")
```
